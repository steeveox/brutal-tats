/**
 * @typedef {Object} updatedSkill
 * @property {number} skillPoints - updated skill's points
 * @property {number} remainingPoints - skillsPoints which were not attributed
 */
class Characters {
	/**
	 * only 1 skillPoint required to increase health by 1
	 * skillPoint / 5 rounded up to increase any other value by 1
	 *
	 * @property {string} skill - skill to update
	 * @property {number} skillPoints - skill's current level
	 * @property {number} allocatedSkillPoints - skillPoints to attribute to the skill
	 * @return updatedSkill
	 */
	static updateSkillPoints(skill, skillPoints, allocatedSkillPoints) {
		let skillPointsToUpdate = skillPoints
		let skillPointsToAdd = allocatedSkillPoints
		if (skill === 'health') {
			return {
				skillPoints: skillPointsToUpdate + skillPointsToAdd,
				remainingPoints: 0,
			}
		}
		while (skillPointsToAdd >= 0) {
			const requiredSkillPoints = skillPointsToUpdate === 0 ? 1 : Math.ceil(skillPointsToUpdate / 5)
			if (requiredSkillPoints > skillPointsToAdd)
				return { skillPoints: skillPointsToUpdate, remainingPoints: skillPointsToAdd }

			skillPointsToUpdate += requiredSkillPoints
			skillPointsToAdd -= requiredSkillPoints
		}

		return { skillPoints, remainingPoints: allocatedSkillPoints }
	}
}

module.exports = Characters
