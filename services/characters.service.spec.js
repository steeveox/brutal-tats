const service = require('./characters.service')

describe('updateSkillPoints', () => {
	it('Should not update skillsPoints if not enough allocatedSkillPoints', () => {
		const test = service.updateSkillPoints('attack', 9, 1)
		expect(test.skillPoints).toBe(9)
		expect(test.remainingPoints).toBe(1)
	})
	it('Should allocate as many skillPoints as possible to health', () => {
		const test = service.updateSkillPoints('health', 10, 10)
		expect(test.skillPoints).toBe(20)
		expect(test.remainingPoints).toBe(0)
	})
	it('Should increase other skills by only 1 for current skillPoints divided by 5, rounded up', () => {
		const test = service.updateSkillPoints('attack', 33, 30)
		expect(test.skillPoints).toBe(58)
		expect(test.remainingPoints).toBe(5)
	})
})
