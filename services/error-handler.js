const { errorHandler } = require('graphql-middleware-error-handler')

const errorHandlerMiddleware = errorHandler({
	onError: (e) => {
		if (e.message.includes('duplicate key error')) {
			if (e.message.includes('username:')) throw new Error('Username already taken')
			if (e.message.includes('name:')) throw new Error('Character name already taken')
			if (e.message.includes('email:')) throw new Error('User email already taken')
		}
		throw new Error(e.message)
	},
	captureReturnedErrors: true,
	forwardErrors: false,
})

module.exports = errorHandlerMiddleware
