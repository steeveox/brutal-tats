const Queries = require('./queries')
const Mutations = require('./mutations')
const User = require('./user')
const Character = require('./character')

module.exports = [User, Character, Queries, Mutations]
