const Mutations = `
type Mutation {
  signup(email: String!, username: String!, password: String!): String!,
  login(email: String, username: String, password: String!): Token!,
  createCharacter(name: String!): Character!,
  updateCharacterSkills(id: ID!, skillsToUpdate: [skillPointsInput!]): Character!,
  deleteCharacter(id: ID!): String!
}`

module.exports = Mutations
