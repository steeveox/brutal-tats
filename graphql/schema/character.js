const Character = `
scalar Date

input skillPointsInput {
  skillPoints: Int!
  skill: String!
}

type Character {
  id: ID!
  name: String!
  rank: Int!
  skillPoints: Int!
  health: Int!
  attack: Int!
  defense: Int!
  magik: Int!
  userId: ID!
  currentHealt: Int
	blockedSince: Date
}`

module.exports = Character
