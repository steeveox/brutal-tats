const Queries = `
type Query {
  getCharacter(id: ID!): Character
  getCharacters: [Character]
  getUser(id: ID!): User
  getUsers: [User]
}`

module.exports = Queries
