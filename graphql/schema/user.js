const User = `
type User {
  id: ID!
  email: String!
  username: String!
  password: String!
}
type Token {
  jwt: ID!
}`

module.exports = User
