/* eslint-disable */
const mongoose = require('mongoose')

mongoose.set('useCreateIndex', true)
mongoose.promise = global.Promise

const dotenv = require('dotenv')
// retrieve env vars
dotenv.config()

async function cleanAllCollections() {
	const collections = Object.keys(mongoose.connection.collections)
	for (const collectionName of collections) {
		const collection = mongoose.connection.collections[collectionName]
		await collection.deleteMany()
	}
}

async function dropAllCollections() {
	const collections = Object.keys(mongoose.connection.collections)
	for (const collectionName of collections) {
		try {
			await mongoose.connection.db.dropCollection(collectionName)
		} catch (error) {
			// Sometimes this error happens, but you can safely ignore it
			if (error.message === 'ns not found') return
			// This error occurs when you use it.todo. You can
			// safely ignore this error too
			if (error.message.includes('a background operation is currently running')) return
			console.log(error.message)
		}
	}
}

module.exports = {
	setupDB(databaseName) {
		beforeAll(async () => {
			const url = `mongodb://${process.env.MONGO_HOST}/${databaseName}`
			await mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
		})

		afterEach(async () => {
			await cleanAllCollections()
		})

		afterAll(async () => {
			await dropAllCollections()
			await mongoose.connection.close()
		})
	},
}
