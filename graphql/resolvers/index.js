const User = require('./users/users')
const Character = require('./characters/characters')
const Date = require('./date')

module.exports = [User, Character]
