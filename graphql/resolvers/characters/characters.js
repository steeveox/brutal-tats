const Character = require('../../../models/character')
const service = require('../../../services/characters.service')

/**
 * @typedef {Object} Character
 * @property {string} string - id of the character
 * @property {string} name - name of the character
 * @property {number} rank - rank of the character
 * @property {number} skillPoints - skillPoints left to be attributed to skills
 * @property {number} health - general life points of the character
 * @property {number} currentHealth - current life points of the character if engaged in combat
 * @property {number} attack - combat force of the character
 * @property {number} defense - combat defense of the character
 * @property {number} magik - combat magik of the character
 * @property {string} userId - id of the character's owner
 * @property {Date} blockedSince - filled if character lost a combat
 */

/**
 * @typedef {Object} skillsToUpdate
 * @property {string} skill - skill to update
 * @property {number} skillPoints - amount of skillPoints to attribute to the skill
 */
module.exports = {
	Query: {
		/**
		 *
		 * @return {Array.<Character>} - list of the current user's characters
		 */
		getCharacters: async (_, {}, context) => {
			if (!context.userId) throw new Error('You must be authenticated!')
			return Character.find({ userId: context.userId, deletedAt: null })
		},

		/**
		 *
		 * @param {string} id - id of the character we try to fecth
		 * @return {Character}
		 */
		getCharacter: async (_, { id }, context) => {
			if (!context.userId) throw new Error('You must be authenticated!')

			const character = await Character.findOne({ _id: id, deletedAt: null })

			if (!character) throw new Error('Looking for a non-existing or deleted character')
			if (String(character.userId) !== String(context.userId))
				throw new Error('You can only see your characters')

			return character
		},
	},

	Mutation: {
		/**
		 *
		 * @param {string} name - name of the character we try to create
		 * @return { Character }
		 */
		createCharacter: async (_, { name }, context) => {
			if (!context.userId) throw new Error('You must be authenticated!')

			const userCharacters = await Character.find({ userId: context.userId })
			if (userCharacters.length >= 10) {
				throw new Error('You can not have more than 10 characters')
			}

			const payload = {
				userId: context.userId,
				name,
			}
			const character = new Character(payload)

			return character.save()
		},

		/**
		 *
		 * @param { string } id - id of the character we try to update
		 * @param { Array.<skillsToUpdate> } skillsToUpdate - array of skill/skillPoints pair
		 * @return { Character }
		 */
		updateCharacterSkills: async (_, { id, skillsToUpdate }, context) => {
			if (!context.userId) throw new Error('You must be authenticated!')

			const updatableSkills = ['health', 'attack', 'defense', 'magik']

			const character = await Character.findById(id)
			if (String(character.userId) !== String(context.userId))
				throw new Error('You can only update your own characters')

			const cumulatedPoints = skillsToUpdate.reduce((acc, sp) => acc + sp.skillPoints, 0)

			if (cumulatedPoints > character.skillPoints)
				throw new Error('Trying to use more skillPoints than the character has')

			/* eslint-disable */ 
			for ({ skillPoints, skill } of skillsToUpdate) {
				if (!updatableSkills.includes(skill)) throw new Error('Trying to update unknown skill')
				const updatedSkill = service.updateSkillPoints(skill, character[skill], skillPoints)
				character[skill] = updatedSkill.skillPoints
				character.skillPoints = character.skillPoints - skillPoints + updatedSkill.remainingPoints
				await character.save()
			}
			/* eslint-enable */

			return character
		},

		/**
		 *
		 * @param {string} id - id of the character we try to create
		 * @return { string } - message confirming the character was deleted
		 */
		deleteCharacter: async (_, { id }, context) => {
			if (!context.userId) throw new Error('You must be authenticated!')

			const character = await Character.findById(id)
			if (String(character.userId) !== String(context.userId))
				throw new Error('You can only delete your characters')

			await character.update({ deletedAt: new Date() })

			return 'Character has been deleted!'
		},
	},
}
