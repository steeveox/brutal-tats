const { ObjectId } = require('mongodb')
const Character = require('../../../models/character')
const resolvers = require('./characters')
const { setupDB } = require('../test-setup')
const service = require('../../../services/characters.service')

setupDB('characters')
const userId = new ObjectId()

describe('getCharacters', () => {
	it('Should throw if not authenticated', async () => {
		await expect(resolvers.Query.getCharacters({}, { userId }, {})).rejects.toThrow(
			/must be authenticated/
		)
	})
	it("Should only retrieve logged user's characters", async () => {
		const newCharacter = new Character({ name: 'steeveo', userId })
		await newCharacter.save()
		const otherUsersCharacter = new Character({ name: 'steeveox', userId: new ObjectId() })
		await otherUsersCharacter.save()

		const characters = await resolvers.Query.getCharacters({}, { userId }, { userId })

		expect(characters).toHaveLength(1)
		expect(characters[0].id).toBe(newCharacter.id)
	})
})

describe('getCharacter', () => {
	it('Should throw if not authenticated', async () => {
		await expect(resolvers.Query.getCharacter({}, { id: new ObjectId() }, {})).rejects.toThrow(
			/must be authenticated/
		)
	})
	it('Should throw if character is deleted', async () => {
		const newCharacter = new Character({ name: 'steeveo', userId, deletedAt: new Date() })
		await newCharacter.save()

		await expect(
			resolvers.Query.getCharacter({}, { id: newCharacter.id }, { userId })
		).rejects.toThrow(/deleted character/)
	})
	it("Should throw if other user's character", async () => {
		const newCharacter = new Character({ name: 'steeveo', userId: new ObjectId() })
		await newCharacter.save()

		await expect(
			resolvers.Query.getCharacter({}, { id: newCharacter.id }, { userId })
		).rejects.toThrow(/your characters/)
	})
	it('Should return required character', async () => {
		const newCharacter = new Character({ name: 'steeveo', userId })
		await newCharacter.save()
		const otherCharacter = new Character({ name: 'steeveox', userId })
		await otherCharacter.save()

		const character = await resolvers.Query.getCharacter({}, { id: newCharacter.id }, { userId })

		expect(character.id).toBe(newCharacter.id)
	})
})

describe('createCharacter', () => {
	it('Should throw if not authenticated', async () => {
		await expect(resolvers.Mutation.createCharacter({}, { name: 'steeveo' }, {})).rejects.toThrow(
			/must be authenticated/
		)
	})
	it('Should throw if user already has 10 characters', async () => {
		/* eslint no-await-in-loop: "off" */
		for (let i = 0; i < 10; i += 1) {
			const newCharacter = new Character({ name: `steeveo${i}`, userId })
			await newCharacter.save()
		}
		await expect(
			resolvers.Mutation.createCharacter({}, { name: 'steeveo' }, { userId })
		).rejects.toThrow(/more than 10 characters/)
	})
	it('Should create character with all properties', async () => {
		const emptyCharacters = await Character.find()
		expect(emptyCharacters).toHaveLength(0)

		await resolvers.Mutation.createCharacter({}, { name: 'steeveo' }, { userId })

		const characters = await Character.find()
		expect(characters).toHaveLength(1)
		expect(characters[0].name).toBe('steeveo')
	})
})

describe('deleteCharacter', () => {
	it('Should throw if not authenticated', async () => {
		await expect(
			resolvers.Mutation.deleteCharacter({}, { id: new ObjectId() }, {})
		).rejects.toThrow(/must be authenticated/)
	})
	it("Should throw if other user's character", async () => {
		const newCharacter = new Character({ name: 'steeveo', userId: new ObjectId() })
		await newCharacter.save()

		await expect(
			resolvers.Mutation.deleteCharacter({}, { id: newCharacter.id }, { userId })
		).rejects.toThrow(/your characters/)
	})
	it('Should soft delete character', async () => {
		const newCharacter = new Character({ name: 'steeveo', userId })
		await newCharacter.save()

		await resolvers.Mutation.deleteCharacter({}, { id: newCharacter.id }, { userId })

		const updatedCharacter = await Character.find({ name: 'steeveo' })
		expect(updatedCharacter.deletedAt).not.toBeNull()
	})
})

describe('updateCharacterSkills', () => {
	it('Should throw if not authenticated', async () => {
		await expect(
			resolvers.Mutation.updateCharacterSkills({}, { id: new ObjectId() }, {})
		).rejects.toThrow(/must be authenticated/)
	})
	it("Should throw if other user's character", async () => {
		const newCharacter = new Character({ name: 'steeveo', userId: new ObjectId() })
		await newCharacter.save()

		await expect(
			resolvers.Mutation.updateCharacterSkills({}, { id: newCharacter.id }, { userId })
		).rejects.toThrow(/your own characters/)
	})
	it('Should throw if user tries to use more skillPoints than the character has', async () => {
		const newCharacter = new Character({ name: 'steeveo', userId, skillPoints: 20 })
		await newCharacter.save()

		await expect(
			resolvers.Mutation.updateCharacterSkills(
				{},
				{
					id: newCharacter.id,
					skillsToUpdate: [
						{ skill: 'attack', skillPoints: 10 },
						{ skill: 'health', skillPoints: 10 },
						{ skill: 'defense', skillPoints: 10 },
					],
				},
				{ userId }
			)
		).rejects.toThrow(/more skillPoints/)
	})
	it('Should throw if user tries to update unknown skill', async () => {
		const newCharacter = new Character({ name: 'steeveo', userId, skillPoints: 20 })
		await newCharacter.save()

		await expect(
			resolvers.Mutation.updateCharacterSkills(
				{},
				{
					id: newCharacter.id,
					skillsToUpdate: [
						{ skill: 'attack', skillPoints: 10 },
						{ skill: 'plop', skillPoints: 10 },
					],
				},
				{ userId }
			)
		).rejects.toThrow(/unknown skill/)
	})
	it("Should update skills and skillPoints according to the service's return values", async () => {
		const newCharacter = new Character({ name: 'steeveo', userId, skillPoints: 20, health: 10 })
		await newCharacter.save()

		jest.spyOn(service, 'updateSkillPoints').mockReturnValue({
			skillPoints: 20,
			remainingPoints: 2,
		})

		await resolvers.Mutation.updateCharacterSkills(
			{},
			{
				id: newCharacter.id,
				skillsToUpdate: [{ skill: 'health', skillPoints: 10 }],
			},
			{ userId }
		)

		const updatedCharacter = await Character.findById(newCharacter.id)
		expect(updatedCharacter.health).toBe(20)
		expect(updatedCharacter.skillPoints).toBe(12)
	})
})
