const mongoose = require('mongoose')

const { Schema } = mongoose

const characterSchema = new Schema(
	{
		name: {
			type: String,
			required: true,
		},
		rank: {
			type: Number,
			required: true,
			min: 1,
			default: 1,
		},
		skillPoints: {
			type: Number,
			required: true,
			min: 0,
			default: 12,
		},
		health: {
			type: Number,
			required: true,
			min: 10,
			default: 10,
		},
		attack: {
			type: Number,
			required: true,
			min: 0,
			default: 0,
		},
		defense: {
			type: Number,
			required: true,
			min: 0,
			default: 0,
		},
		magik: {
			type: Number,
			required: true,
			min: 0,
			default: 0,
		},
		userId: {
			type: Schema.Types.ObjectId,
			ref: 'User',
			required: true,
		},
		// filled if the character is in the middle of a fight
		currentHealth: {
			type: Number,
			required: false,
			default: null,
		},
		// filled if the character is blocked (losed a fight)
		// a character is blocked for 1h from the blockedSince date
		blockedSince: {
			type: Date,
			required: false,
			default: null,
		},
		deletedAt: {
			type: Date,
			required: false,
			default: null,
		},
	},
	{ timestamps: true }
)

// name must be unique, but as we soft-delete
// we create a compound index on name AND deletedAt
// this way a new character can still have a name used by a deleted one
characterSchema.index({ name: 1, deletedAt: 1 }, { unique: true })

module.exports = mongoose.model('Character', characterSchema)
